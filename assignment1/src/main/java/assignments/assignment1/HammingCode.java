package assignments.assignment1;

import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    public static String encode(String data) {
        //mencari redundan
        int r = 1; 
        while (Math.pow(2, r) < (data.length() + r + 1))
            r++; 
        
        //membuat array data dengan bit pada redundan diganti 0
        int[] code = new int[r+data.length()];
        int j = 0;
        int k = 0;
        int lipatan_r = 0;
        for (int i = 0; i < code.length; i++){ 
            if (i == j){
                code[i] = 0;
            }else {
                code[i] = (int) Character.getNumericValue(data.charAt(k));
                k++;
                continue;
            }
            j = (int) (j + Math.pow(2, lipatan_r));
            lipatan_r++;
        }

        //menemukan letak kesalahan bit pada posisi redundan
        int[] kesalahan = generate(code, r);

        //mengganti kesalahannya berdasarkan letak 
        //sebelumnya redundan bernilai 0, sehingga diubah jadi 1
        for (int p = 0; p < kesalahan.length; p++){
            if (kesalahan[p] != 0)
                code[kesalahan[p]-1] = 1;
        }
        //mengembalikan data dalam bentuk code
        return print(code);
    }

    public static String decode(String code) {
        //mencari nilai redundan
        int r = (int) Math.ceil(Math.log(code.length()+1)/Math.log(2));

        //membuat array codenya (data_sementara) untuk dicek kesalahannya
        int[] data_sementara = new int[code.length()]; 
        for (int i = 0; i < code.length(); i++)
            data_sementara[i] = (int) Character.getNumericValue(code.charAt(i));
        int[] kesalahan = generate(data_sementara, r);

        //mencari letak kesalahan bit
        int salahBit = -1;
        for (int j = 0; j < r; j++)
            salahBit += kesalahan[j];
        
        //mengganti bit tersebut
        if (salahBit != -1){
            switch(data_sementara[salahBit]){
                case 0: 
                    data_sementara[salahBit] = 1;
                    break;
                case 1: 
                    data_sementara[salahBit] = 0;
                    break;
            }
        }
        
        //membuat array berupa data dari code (data_sementara)
        int[] data_benar = new int[data_sementara.length-r];
        int j = 0;
        int k = 0;
        int lipatan_r = 0;
        for (int i = 0; i < data_sementara.length; i++){ 
            if (i == j){
                j = (int) (j + Math.pow(2, lipatan_r));
                lipatan_r++;
            }else {
                data_benar[k] = (int) data_sementara[i];
                k++;
            }
        }
        return print(data_benar);
    }

    //membuat method untuk mencari letak kesalahan bit
    public static int[] generate(int[] myArr, int r){
        int[] kesalahan = new int[r];
        int kelipatan = 0;
        while (kelipatan < r){
            int q = (int) Math.pow(2, kelipatan);
            int s = 0;
            int x = 0;
            while (x < q){
                for (int i = q+x-1; i < myArr.length; i+=(q*2)){
                    s += myArr[i];
                }
                x++;
            }
            if (s % 2 == 1){
                kesalahan[kelipatan] = q;
            }
            kelipatan++;
        }
        return kesalahan;
    }

    //membuat method untuk mengembalikan bit dalam array dalam bentuk string
    public static String print(int[] arr){ 
        String gabung = "";
        for (int i = 0; i < arr.length; i++){
            switch(arr[i]){
                case 0:
                    gabung += "0";
                    break;
                case 1:
                    gabung += "1";
                    break;
            }
        }
        return gabung;
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
