package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        // TODO: buat constructor untuk ButirPenilaian.
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    public double getNilai() {
        // TODO: kembalikan nilai yang sudah disesuaikan dengan keterlambatan.{
        if (this.terlambat == true){
            return this.nilai * ((100-PENALTI_KETERLAMBATAN)/100.00);
        }else{
            return this.nilai;
        }
    }


    @Override
    public String toString() {
        // TODO: kembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        
        if (this.terlambat == true){
            return String.format("%.2f (T)", getNilai());
        }else{
            return String.format("%.2f", getNilai());
        }
    }
}
