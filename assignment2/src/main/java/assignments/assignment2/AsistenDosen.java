package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        // TODO: buat constructor untuk AsistenDosen.
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        // TODO: kembalikan kode AsistenDosen.
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // TODO: tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        // TODO: kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (Mahasiswa i : mahasiswa){
            if (i.getNpm().equals(npm)){
                return i;
            }
        }
        return null;
    }

    public String rekap() {
        String c = "~".repeat(15) + "\n";
        String d = "";
        for (Mahasiswa j : this.mahasiswa){
            d += j.toString() + "\n" + j.rekap() + "\n";
        }
        return c + d;
    }

    public String toString() {
        return this.kode + " - " + this.nama;
    }
}
