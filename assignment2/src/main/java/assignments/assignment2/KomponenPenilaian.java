package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // TODO: buat constructor untuk KomponenPenilaian.(clear)
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // TODO: masukkan butir ke butirPenilaian pada index ke-idx. (clear)
        this.butirPenilaian[idx] = butir;
    }

    public String getNama() {
        // TODO: kembalikan nama KomponenPenilaian. (clear)
        return this.nama;
    }

    public double getRerata() {
        // TODO: kembalikan rata-rata butirPenilaian. (clear)
        int jumlah_berisi = 0;
        double total = 0;
        for (ButirPenilaian i: this.butirPenilaian){
            if (i != null){
                total += i.getNilai();
                jumlah_berisi++;
            }
        }
        if (jumlah_berisi == 0){
            jumlah_berisi = 1;
        }
        double rerata = total/jumlah_berisi;
        return rerata;
    }

    public double getNilai() {
        // TODO: kembalikan rerata yang sudah dikalikan dengan bobot. (clear)
        double rata_bot = getRerata() * (this.bobot/100.00);
        return rata_bot;
    }

    public String getDetail() {
        // TODO: kembalikan detail KomponenPenilaian sesuai permintaan soal. (clear)
        String a = String.format("\n~~~%s(%d%%)~~~\n", this.nama, this.bobot);
        String b = "";
        String c = "";
        int i = 0;
        for (ButirPenilaian j : butirPenilaian){
            if (j != null){
                if (this.butirPenilaian.length > 1){
                    c = toString();
                    i++;
                    b += String.format("%s %d : %s\n", this.nama, i, j.toString());
                }else{
                    b += String.format("%s: %s", this.nama, j.toString());
                    break;
                }
            }
        }
        String d = String.format("\nKontribusi nilai akhir: %.2f\n", getNilai());
        return a + b + c + d;
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal. (clear)
        return String.format("Rerata %s: %.2f", getNama(), getRerata());
    }

}
