package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // TODO: buat constructor untuk Mahasiswa. (clear)
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // TODO: kembalikan KomponenPenilaian yang bernama namaKomponen.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (KomponenPenilaian i : this.komponenPenilaian){
            if (i.getNama().equals(namaKomponen)){
                return i;
            }
        }
        return null;
    }
    public String getNpm() {
        // TODO: kembalikan NPM mahasiswa. (clear)
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // TODO: kembalikan rekapan sesuai dengan permintaan soal.
        String a = "";
        double total_nilai = 0;
        for (KomponenPenilaian j : this.komponenPenilaian){
            a += j.toString() + "\n";
            total_nilai += j.getNilai();
        }
        String b = "\nNilai akhir: " + total_nilai;
        String c = "\nHuruf: " + getHuruf(total_nilai);
        String d = "\n" + getKelulusan(total_nilai);
        return a + b + c + d;
    }

    public String toString() {
        // TODO: kembalikan representasi String dari Mahasiswa sesuai permintaan soal. (clear)
        return String.format("%s - %s", this.npm, this.nama);
    }

    public String getDetail() {
        // TODO: kembalikan detail dari Mahasiswa sesuai permintaan soal.\
        String a = "";
        for (KomponenPenilaian j : this.komponenPenilaian){
            a += j.getDetail() + "\n";
        }
        double total_nilai = 0;
        for (KomponenPenilaian j : this.komponenPenilaian){
            a += j.toString() + "\n";
            total_nilai += j.getNilai();
        }
        String b = "\nNilai akhir: " + total_nilai;
        String c = "\nHuruf: " + getHuruf(total_nilai);
        String d = "\n" + getKelulusan(total_nilai);
        return a + b + c + d;
    }
 
    @Override
    public int compareTo(Mahasiswa other) {
        // TODO: definisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        return this.npm.compareTo(other.npm);
    }
}
