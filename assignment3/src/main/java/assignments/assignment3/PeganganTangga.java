package assignments.assignment3;

public class PeganganTangga extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
  	
    public PeganganTangga(String name){
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(name);
    }

    public void tambahPersentase(){
        this.setPersentaseMenular(this.getPersentaseMenular()+20);
    }
    
    public String toString(){
        return "PEGANGAN TANGGA " + this.getNama();
    }
}