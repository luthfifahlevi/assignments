package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
        jumlahDisembuhkan = 0;
    }

    public void obati(Manusia manusia) {
        // TODO: Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Hint: Update nilai atribut jumlahDisembuhkan
        manusia.ubahStatus("Negatif");
        jumlahDisembuhkan++;
        for(Carrier penular : manusia.getRantaiPenular()){
            penular.ubahStatus("");
        }
    }

    public int getJumlahDisembuhkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }

    public String toString(){
        return "PETUGAS MEDIS " + this.getNama();
    }
}