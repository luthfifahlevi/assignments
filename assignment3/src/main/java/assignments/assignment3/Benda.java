package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular;

    public Benda(String name){
        // TODO: Buat constructor untuk Benda.
        // Hint: Akses constructor superclass-nya
        super(name, "Benda");
        persentaseMenular = 0;
    }

    public abstract void tambahPersentase();

    public int getPersentaseMenular(){
        // TODO : Kembalikan nilai dari atribut persentaseMenular
        return persentaseMenular;
    }
    
    public void setPersentaseMenular(int persentase) {
        // TODO : Gunakan sebagai setter untuk atribut persentase menular
        persentaseMenular = persentase;
    }

    public abstract String toString();
}