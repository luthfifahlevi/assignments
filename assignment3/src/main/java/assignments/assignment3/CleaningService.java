package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        // TODO: Buat constructor untuk CleaningService.
        // Hint: Akses constructor superclass-nya
        super(nama);
        jumlahDibersihkan = 0;
    }

    public void bersihkan(Benda benda){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        benda.setPersentaseMenular(0);
        if(benda.getStatusCovid().equalsIgnoreCase("Positif")){
            benda.ubahStatus("Negatif");
            for(Carrier penular : benda.getRantaiPenular()){
                penular.ubahStatus("");
            }
        }
        jumlahDibersihkan++;
    }

    public int getJumlahDibersihkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return jumlahDibersihkan;
    }

    public String toString(){
        return "CLEANING SERVICE " + this.getNama();
    }

}