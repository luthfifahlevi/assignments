package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        // TODO: Buat constructor untuk class World
       listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama){
        // TODO: Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        if(tipe.equalsIgnoreCase("OJOL")){
            Carrier pelaku = new Ojol(nama);
            return pelaku;
        }else if(tipe.equalsIgnoreCase("PEKERJA_JASA")){
            Carrier pelaku = new PekerjaJasa(nama);
            return pelaku;
        }else if(tipe.equalsIgnoreCase("JURNALIS")){
            Carrier pelaku = new Jurnalis(nama);
            return pelaku;
        }else if(tipe.equalsIgnoreCase("PETUGAS_MEDIS")){ 
            Carrier pelaku = new PetugasMedis(nama);
            return pelaku;
        }else if(tipe.equalsIgnoreCase("CLEANING_SERVICE")){
            Carrier pelaku = new CleaningService(nama);
            return pelaku;
        }else if(tipe.equalsIgnoreCase("PEGANGAN_TANGGA")){
            Carrier pelaku = new PeganganTangga(nama);
            return pelaku;
        }else if(tipe.equalsIgnoreCase("ANGKUTAN_UMUM")){
            Carrier pelaku = new AngkutanUmum(nama);
            return pelaku;
        }else if(tipe.equalsIgnoreCase("TOMBOL_LIFT")){
            Carrier pelaku = new TombolLift(nama);
            return pelaku;
        }else{
            Carrier pelaku = new Pintu(nama);
            return pelaku;
        }
    }

    public Carrier getCarrier(String nama){
        // TODO: Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for(Carrier i : listCarrier){
            if(i.getNama().equalsIgnoreCase(nama)){
                return i;
            }
        }
        return null;
    }
}
