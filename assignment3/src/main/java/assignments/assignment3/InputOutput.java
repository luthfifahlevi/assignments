package assignments.assignment3;

import java.io.*;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        // TODO: Buat constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
        world = new World();
        
    }

    public void setBufferedReader(String inputType){
        // TODO: Membuat BufferedReader bergantung inputType (I/O text atau input terminal) 
        try{
            if(inputType.equalsIgnoreCase("TEXT")){
                br = new BufferedReader(new FileReader(this.inputFile));
            }else{
                br = new BufferedReader(new InputStreamReader(System.in));
            }
        }catch(IOException e){
            System.out.println("File not found.");
        }
    }
    
    public void setPrintWriter(String outputType){
        // TODO: Membuat PrintWriter bergantung inputType (I/O text atau output terminal)
        try{
            if(outputType.equalsIgnoreCase("TEXT")){
                pw = new PrintWriter(new File(this.outputFile));
            }else{
                pw = new PrintWriter(System.out);
            }
        }catch(IOException e){
            System.out.println("Ada kesalahan.");
        }
    }

    public void run() throws IOException{
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesuai dapat gunakan method getCarrier pada class World
        int total_sembuh_manusia = 0;
        String[] perintah = br.readLine().split(" ");
        while(!perintah[0].equalsIgnoreCase("EXIT")){
            Carrier objek = world.getCarrier("");
            if(perintah.length>1){
                objek = world.getCarrier(perintah[1]);
            }
            switch(perintah[0].toUpperCase()){
                case "ADD":
                    world.listCarrier.add(world.createObject(perintah[1], perintah[2]));
                    break;

                case "INTERAKSI":
                    if(objek.getStatusCovid().equals("Positif")){
                        objek.interaksi(world.getCarrier(perintah[2]));
                    }else{
                        (world.getCarrier(perintah[2])).interaksi(objek);
                    }
                    break;

                case "POSITIFKAN":
                    objek.ubahStatus("Positif");
                    break;

                case "SEMBUHKAN":
                    ((PetugasMedis)world.getCarrier(perintah[1])).obati((Manusia)world.getCarrier(perintah[2]));
                    world.getCarrier(perintah[2]).getRantaiPenular().clear();
                    ((PetugasMedis)world.getCarrier(perintah[1])).tambahSembuh();
                    total_sembuh_manusia = ((PetugasMedis)world.getCarrier(perintah[1])).getJumlahSembuh();
                    break;

                case "BERSIHKAN":
                    ((CleaningService)world.getCarrier(perintah[1])).bersihkan((Benda)world.getCarrier(perintah[2]));
                    world.getCarrier(perintah[2]).getRantaiPenular().clear();
                    break;

                case "RANTAI":
                    try{
                        if(objek.getStatusCovid().equals("Positif")){
                            if(objek.getRantaiPenular().size() == 0){
                                pw.println("Rantai penyebaran " + objek.toString() + ": " + objek.toString());
                            }else{
                                String kalimat="";
                                for(Carrier penular : objek.getRantaiPenular()){
                                    kalimat += penular.toString() + " -> ";
                                }
                                pw.println("Rantai penyebaran " + objek.toString() + ": " + kalimat + objek.toString());
                            }
                        }else{
                            throw new BelumTertularException("assignments.assignment3.BelumTertularException: " + objek.toString() + " berstatus negatif");
                        }
                    }catch(BelumTertularException e){
                        pw.println(e.getMessage());
                    }
                    break;
                
                case "TOTAL_KASUS_DARI_OBJEK":
                    pw.println(objek.toString() + " telah menyebarkan virus COVID ke " + objek.getTotalKasusDisebabkan() + " objek");
                    break;
                
                case "AKTIF_KASUS_DARI_OBJEK":
                    pw.println(objek.toString() + " telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak " + objek.getAktifKasusDisebabkan() + " objek");
                    break;

                case "TOTAL_SEMBUH_MANUSIA":
                    pw.println("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: " + total_sembuh_manusia + " kasus");
                    break;
                
                case "TOTAL_SEMBUH_PETUGAS_MEDIS":
                    pw.println(objek.toString() + " menyembuhkan " +  ((PetugasMedis)world.getCarrier(perintah[1])).getJumlahDisembuhkan() + " manusia");
                    break;
                
                case "TOTAL_BERSIH_CLEANING_SERVICE":
                    pw.println(objek.toString() + " membersihkan " +  ((CleaningService)world.getCarrier(perintah[1])).getJumlahDibersihkan() + " benda");
                    break;
            }
            perintah = br.readLine().split(" ");
        }
        pw.close();
        br.close();
    }
    
}