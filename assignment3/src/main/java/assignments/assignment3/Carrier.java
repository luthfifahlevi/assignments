package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama,String tipe){
        // TODO: Buat constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
        statusCovid = new Negatif();
        aktifKasusDisebabkan = 0;
        totalKasusDisebabkan = 0;
        rantaiPenular = new ArrayList<Carrier>();
    }

    public String getNama(){
        // TODO : Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe(){
        // TODO : Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid(){
        // TODO : Kembalikan nilai dari atribut statusCovid
        return statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        return aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        // TODO : Kembalikan nilai dari atribut totalKasusDisebabkan
        return totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular(){
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        return rantaiPenular;
    }

    public void ubahStatus(String status){
        // TODO : Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if(status.equalsIgnoreCase("Positif")){
            statusCovid = new Positif();
        }else if(status.equalsIgnoreCase("Negatif")){
            statusCovid = new Negatif();
        }else if(status.equals("")){
            aktifKasusDisebabkan--;
        }else{
            aktifKasusDisebabkan++;
            totalKasusDisebabkan++;
        }
    }

    public void interaksi(Carrier lain){
        // TODO : Objek ini berinteraksi dengan objek lain
        List<Carrier> Check = new ArrayList<Carrier>();
        if(!statusCovid.getStatus().equals(lain.getStatusCovid())){
            if(lain.getTipe().equalsIgnoreCase("Benda")){
                Benda var = (Benda) lain;
                var.tambahPersentase();
                Carrier git = (Carrier) var;
                if(var.getPersentaseMenular()>=100){
                    statusCovid.tularkan(this, git);          
                    if(rantaiPenular.size()>0){
                        for(Carrier i : rantaiPenular){
                             //mengecek agar tidak ada terulang saat menambahkan total kasus dan aktif kasus
                            if(!Check.contains(i)){
                                Check.add(i);
                                i.ubahStatus("tambah_kasus");
                            }
                            git.getRantaiPenular().add(i);
                        }
                    }
                    git.getRantaiPenular().add(this);
                    if(!Check.contains(this)){
                        Check.add(this);
                        this.ubahStatus("tambah_kasus");
                    }
                }
            }else{
                statusCovid.tularkan(this, lain);
                if(rantaiPenular.size()>0){
                    for(Carrier i : rantaiPenular){
                         //mengecek agar tidak ada terulang saat menambahkan total kasus dan aktif kasus
                        if(!Check.contains(i)){
                            Check.add(i);
                            i.ubahStatus("tambah_kasus");
                        }
                        lain.getRantaiPenular().add(i);
                    }
                }
                lain.getRantaiPenular().add(this);
                if(!Check.contains(this)){
                    Check.add(this);
                    this.ubahStatus("tambah_kasus");
                }
            }
        }
    }

    public abstract String toString();

}
